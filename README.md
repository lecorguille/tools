# IFB clusters tools deployment

This repository allows to manage the deployment of tools for the IFB clusters federation.

# Add a tool

1. Add a new tool or tool version definition (see [Tool definition format](#tool-definition-format)). There are examples of well-written definition in ["tools/sample"](https://gitlab.com/ifb-elixirfr/cluster/tools/tree/master/tools/sample) you can copy.
2. Propose the change as a merge request. Your definition will automatically be linted and deployed on a docker environment to test that it works, but the tool will not yet be available on the IFB clusters federation.
3. Once the definition is ready it will be merged and deployed automatically to the federated clusters.

**Tips:** To interact with a GitLab repository, you can use both the [web interface](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or git through [command lines](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line).

**Note:** If you aren't part of this repository developer, GitLab will [fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) this repo for you on your account.

# Conda quick mode

For Conda packages that come from either [`bioconda`](https://bioconda.github.io/) or [`conda-forge`](https://anaconda.org/conda-forge), you just have to add one line in one file.
You can use both the [web interface](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or git through [command lines](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line)

Example: tools/braker2/2.1.4/meta.yml
```
deployment: conda
```

Our :robot: [@ifbot](https://gitlab.com/ifbot) will process his magic to complete the [`meta.yml`](https://gitlab.com/ifb-elixirfr/cluster/tools/-/blob/master/tools/braker2/2.1.4/meta.yml) and the [`env.yml`](https://gitlab.com/ifb-elixirfr/cluster/tools/-/blob/master/tools/braker2/2.1.4/env.yml)
And eventually 

**If:** ✅ 
One of the mainteners will merge your Merge Resquest and trigger the installation on the different production infrastructures.


**Else:** 💣you get this kind of message:
```my tools v1.1
✨ I added a conda environment definition
💣 Incomplete meta data and unable to complete them automatically
```
You may have to complete the [description](https://gitlab.com/ifb-elixirfr/cluster/tools/-/blob/master/tools/pipmir/1.1/meta.yml#L3-4) on the same branch.

:warning: Make sure to allow the maintainers of the tools repository to push on your branch in the Merge Request settings if you want the :robot: [@ifbot](https://gitlab.com/ifbot) to complete your contribution.


# Command line script to help deploying the Conda tool

The repository include a script to let you create quickly the definition of conda deployment.

This tools requires the anaconda tool (anaconda-client) in your PATH.

```
bash add_conda_tool <channel>/<software>/<version>
```

The script will generate automatically the two files required for the deployment definition. You will just have to push them in your repository and create a merge request.

# Tool definition format

Each tool version to deploy must be defined by :
- A `meta.yml` file that give general information on the software and how it should be deployed
- A definition file that is either a conda environment or a Singularity image definition

The `meta.yml` accepts the following fields :

**Common**

| Property          | Mandatory | Description                              |
|-------------------|-----------|------------------------------------------|
| deployment        |     X     | Type of tool (conda or singularity)      |
| about.description |     X     | One line description of the tool         |
| about.url         |           | Homepage URL of the tool                 |
| require           |           | List of required module to use this tool |

**Singularity**

When using a Singularity deployment, wrappers will be created for all binaries that should be made available for the tools. The list of binaries and execution options can be defined within the `meta.yml` file.

| Property         | Mandatory | Description                                                                                                   |
|------------------|-----------|---------------------------------------------------------------------------------------------------------------|
| binaries         |           | List of binaries that should be wrapped                                                                       |
| binaries_paths   |           | List of folder containing binaries that should be wrapped                                                     |
| binds            |           | List of binds that should be enabled for all wrappers. Each bind must have a `src` and `dest` property.   |
| with_gpu_support |           | Enable NVidia support in Singularity for all wrappers                                                         |
| make_env_wrapper |           | Build a special wrapper called run_with_`software` to run script or software inside the container environment |

When listing binaries that should be wrapped, you can add a specific `binds` list for each binary.
