Bootstrap: docker
From: ubuntu:20.04

# based on https://github.com/yfukasawa/LongQC/blob/master/Dockerfile

%labels
    Author IFB
    Version master

%environment
  export PATH=/opt/LongQC:/opt/conda/bin:$PATH

%post
    export PATH=/opt/LongQC:/opt/conda/bin:$PATH
    export DEBIAN_FRONTEND=noninteractive
    # prepare debian/ubuntu
    apt-get update && apt-get upgrade -y
    apt-get install -y  \
            apt-utils \
            apt-transport-https \
            ca-certificates \
            curl \
            gnupg-agent \
            software-properties-common \
            sed \
            wget \
            git \
            build-essential \
            libc6-dev \
            zlib1g-dev
    apt-get clean
    apt-get purge

    # install conda
    wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    ./Miniconda3-latest-Linux-x86_64.sh -b -s -p /opt/conda

    # install dependency
    conda update -y conda
    conda install -c conda-forge -y mamba
    mamba install -y -c conda-forge -c bioconda -c defaults\
          numpy \
          pandas'>=0.24.0' \
          scipy \
          jinja2 \
          h5py \
          matplotlib'>=2.1.2' \
          scikit-learn \
          pysam \
          edlib \
          python-edlib

    # get longqc
    git clone https://github.com/yfukasawa/LongQC.git /opt/LongQC
    cd /opt/LongQC/minimap2_mod && make extra
    chmod +x /opt/LongQC/longQC.py
    # should be removed one day
    cd /opt/LongQC/ && sed -i.bak '1 i #!/opt/conda/bin/python' longQC.py


%runscript
    exec longQC.py "$@"

%test
    export PATH=/opt/LongQC:/opt/conda/bin:$PATH
    longQC.py --version
    longQC.py --help
